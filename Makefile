JAVAC=/usr/bin/javac

SRC = ./src/
BIN = ./bin/
DOC	= ./doc/

# g flag used to show debugging info and d flag moves output to a new directory
JFLAGS = -g -d $(BIN) -cp $(SRC)

# Make the .class file
COMPILE = $(JAVAC) $(JFLAGS)

EMPTY = 

JAVA_FILES = $(subst $(SRC), $(EMPTY), $(wildcard $(SRC)*.java))


ALL_FILES = $(JAVA_FILES)

CLASS_FILES = $(ALL_FILES:.java=.class)

all : $(addprefix $(BIN), $(CLASS_FILES))

# magic
$(BIN)%.class : $(SRC)%.java
	$(COMPILE) $<

docs :
	find $(SRC) -type f -name "*.java" | xargs javadoc -d $(DOC) 

clean: 
	rm -rf $(BIN)*.class
