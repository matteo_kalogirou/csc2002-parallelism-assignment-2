First make the dependencies by typing:
->make
Note that you must be in the same level of the directory as the src folder and the bin folder.

To run the parallel program use the following command structure:
java TreeGrow <realtivepath to the data file>
eg -> java TreeGrow ../data/sample_input.txt

To run the sequential version type:
java SeqTreeGrow <relativepath to the data file>
eg -> java SeqTreeGrow ../data/sample_input.txt
