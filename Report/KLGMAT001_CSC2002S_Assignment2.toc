\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Classes}{3}
\contentsline {subsection}{\numberline {2.1}Tree}{4}
\contentsline {subsubsection}{\numberline {2.1.1}sunexposure()}{4}
\contentsline {subsubsection}{\numberline {2.1.2}sungrow()}{5}
\contentsline {subsubsection}{\numberline {2.1.3}getPositions()}{5}
\contentsline {subsection}{\numberline {2.2}Land}{5}
\contentsline {subsubsection}{\numberline {2.2.1}setShade()}{6}
\contentsline {subsubsection}{\numberline {2.2.2}resetShade()}{6}
\contentsline {subsubsection}{\numberline {2.2.3}shadow()}{6}
\contentsline {subsection}{\numberline {2.3}Model}{7}
\contentsline {subsubsection}{\numberline {2.3.1}simulate()}{7}
\contentsline {subsection}{\numberline {2.4}Simulator}{8}
\contentsline {subsubsection}{\numberline {2.4.1}compute()}{9}
\contentsline {subsection}{\numberline {2.5}Button Listener}{9}
\contentsline {section}{\numberline {3}Model-View-Controller Design}{10}
\contentsline {section}{\numberline {4}Java Concurrency Features}{11}
\contentsline {subsection}{\numberline {4.1}Atomic Variables}{11}
\contentsline {subsection}{\numberline {4.2}Synchronized Statements and Methods}{12}
\contentsline {section}{\numberline {5}Code Details}{13}
\contentsline {subsection}{\numberline {5.1}Thread Safety and Synchronisation}{13}
\contentsline {subsection}{\numberline {5.2}Liveness and Deadlock}{14}
\contentsline {section}{\numberline {6}Validation}{15}
\contentsline {section}{\numberline {7}Discussion}{17}
\contentsline {section}{\numberline {A}Code}{i}
