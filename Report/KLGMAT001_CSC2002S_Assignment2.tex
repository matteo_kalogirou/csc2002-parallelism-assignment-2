\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage{natbib}
\usepackage{url}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{{images/}}
\usepackage{parskip}
\usepackage{fancyhdr}
\usepackage{vmargin}
\usepackage{algorithmic}
%Rename Require and Ensure to input and output

\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}



\usepackage{my_basic_report}

\lstset{style=Ccode} 	%style = C code
\lstset{style=Matlab} 	%style = Matlab

\graphicspath{{/Users/matteokalogirou/Library/texmf/logo/},{../Figures/}}

\setmarginsrb{3 cm}{2.5 cm}{3 cm}{2.5 cm}{1 cm}{1.5 cm}{1 cm}{1.5 cm}

\title{Assignment 2 - Concurrency}								% Title
\author{Matteo Kalogirou}								% Author
\date{\today}											% Date

\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\makeatother

\pagestyle{fancy}
\fancyhf{}
\rhead{\theauthor}
\lhead{\thetitle}
\cfoot{\thepage}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{titlepage}
	\centering
    \vspace*{0.5 cm}
    \includegraphics[scale = 0.75]{uct_logo}\\[1.0 cm]	% University Logo
    \textsc{\LARGE University of Cape Town}\\[2.0 cm]	% University Name
	\textsc{\Large CSC2002S}\\[0.5 cm]				% Course Code
	\textsc{\large Parallel Programming and Concurrency}\\[0.5 cm]				% Course Name
	\rule{\linewidth}{0.2 mm} \\[0.4 cm]
	{ \huge \bfseries \thetitle}\\
	\rule{\linewidth}{0.2 mm} \\[1.5 cm]
	
	\begin{minipage}{0.4\textwidth}
		\begin{flushleft} \large
			\emph{Author:}\\
			\theauthor
			\end{flushleft}
			\end{minipage}~
			\begin{minipage}{0.4\textwidth}
			\begin{flushright} \large
			\emph{Student Number:} \\
			KLGMAT001									% Your Student Number
		\end{flushright}
	\end{minipage}\\[2 cm]
	
	{\large \thedate}\\[2 cm]
 
	\vfill
	
\end{titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\tableofcontents
\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

This assignment involves creating a multithreaded Java program which is used to simulate tree growth in an area. The program must then display the simulated results on a GUI, which is also used to control the simulation. The program will be considered successful if: the multithreaded simulation achieves some speedup over the sequential version while also producing an output which is free from concurrency issues.

This simulation assumes a simplistic approach to tree growth whereby the only factor affecting the growth is the amount of sunlight the tree is exposed to. Additionally, this simulation considers the effects of sunlight filtration through tree canopies. This implies that only a fraction of the total sunlight is filtered through each tree's canopy, in turn affecting the growth rate of smaller trees which lie in the shade of larger trees.

The program is provided with an input file that describes: the amount of sunlight at each point in an area, hereon referred to as the \emph{sunmap}, and a list of each tree in the area. Each tree is described by its \emph{x, y} co-ordinate and its \emph{extent}.

A high-level overview for one simulation cycle of the program can then be described as follows:
\begin{itemize}
	\item For each layer of tree canopy starting from the largest trees and moving through to the smallest trees:
		\begin{itemize}
			\item Calculate the total sunlight they receive from the \emph{sunmap} based on tree position and size
			\item Average the received sunlight and grow the tree by a proportional amount
			\item Cast shade on the \emph{sunmap} by reducing the sunlight under the tree by a fraction
		\end{itemize}
	\item Reset the \emph{sunmap} to the original value
\end{itemize}

One cycle of the above process corresponds to one simulated "year". Additionally it is assumed that the sunlight exposure remains the same for every year.


% Produce a parallel concurrent threaded program to speed  up the implementation -> compared against a sequential version
%Have multiple threads accessing the shared data which cause problems of concurrency to arice such as interleaving, race  onditions and dedlocks. This program attempts to simulate the large data set as quickly aas possible and test for whether race conditions occur.

A description of the classes that are used to run the program can be seen in Section\,\ref{sec: classes}. This program attempts to conform to the Model-View-Controller (MVC) design pattern. The implementation of this design and the corresponding class interactions are described in Section\,\ref{sec: mvc}. The Java tools used to prevent concurrency issues are outlined in Section\,\ref{sec: conc_features}. An overview of how the code works and the thread safety precautions are then described in Section\,\ref{sec: code}. Finally, Section\,\ref{sec: validate} evaluates the successfulness of the program based on the success criteria.

%==========================CLASSES
\section{Classes}\label{sec: classes}

There are a number of different classes which the program uses to simulate the growth of trees. This Section will describe the additional classes required by the program as well as outlining the modifications and additions made to the existing classes. A class diagram of the system can be seen in Figure\,\ref{fig: class_diagram}.

\begin{figure}[h]
\begin{centering}
	\includegraphics[width = \textwidth]{./Figures/class_diagram.png}
	\caption{Class diagram of the tree growth simulator program.}
	\label{fig: class_diagram}
\end{centering}
\end{figure}

%--------------------Tree
\subsection{Tree}
The \emph{Tree} class is used to describe a tree in the simulated forest. Each \emph{Tree} object is described by its:
\begin{itemize}
	\item \textbf{xpos}: the \emph{x} co-ordinate of the tree trunk
	\item \textbf{ypos}: the \emph{y} co-ordinate of the tree trunk
	\item \textbf{ext}: the size of the tree measured outwards from the tree trunk.
	\item \textbf{positions}: a four element array which describes the tree's start and end points on the \emph{sunmap} based on its \emph{x, y} co-ordinates and extent.
\end{itemize}

Each \emph{Tree} object is primarily used to find the total sunlight incident upon the tree, \emph{sunexposure()} and to then grow the tree by a proportional amount, \emph{sungrow()}. These predominant methods are discussed in the subsequent Sections whereas the straightforward accessor and mutator methods are not discussed.

	\subsubsection{sunexposure()}
This method first makes a call to the helper method, \emph{getPostions()}, see Section \,\ref{meth: getPostitions}. This indicates the boundaries of the tree's extent on the \emph{shademap}. Using these start and end points, then sums the total sunlight received by the tree and returns the average sunlight. The average is calculated by dividing the total sunlight by the number of cells the tree covers. This is described by Algorithm\,\ref{alg: sunexposure}.
\begin{algorithm}
\floatname{algorithm}{Procedure}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
	\caption{Method used to calculate the averaged amount of sunlight received by a tree.}
	\label{alg: sunexposure}
	\begin{algorithmic}
		\REQUIRE Land object and its shademap
		\ENSURE Average amount of sunlight received by the tree
		\STATE getPositions() \COMMENT {Get the start and end indexes of the tree on the sunmap}
		\STATE $sum \Leftarrow 0$
		\FOR{every xpos}
			\STATE{Lock the shademap at row x}
			\FOR{every ypos}
				\STATE{sum $\Leftarrow$ sum +  getShade[xpos][ypos] }
			\ENDFOR
			\STATE{Unlock the shademap at row x}
		\ENDFOR
		\RETURN $sum / (extent\times2+1)^2$ 
	\end{algorithmic}
\end{algorithm}

The \emph{sunexposure()} method requires read access to the shared memory resource, \emph{shademap}. For this reason, the method has been synchronised around the active row of the \emph{shademap}. This implies that as the algorithm moves through each row of the tree's extent; that entire row is exclusively accessible by this instance of the calling method.

	\subsubsection{sungrow()} \label{meth: sungrow}
This method is used to grow the tree by an amount proportional to the averaged sunlight that is calculated using \emph{sunexposre()}, \emph{sungrow()} is therefore, the calling method of \emph{sunexposure()}. A tree's growth factor is $s/1000$ where $s$ is the averaged sunlight calculated by \emph{sunexposure()}.
Once the result has been calculated, the mutator method \emph{setExt()} is used to set the new extent of the tree. 
	
	\subsubsection{getPositions()} \label{meth: getPostitions}
This method is a helper method used by \emph{sunexposure()}. It returns a four element array which represents: the \emph{xpos} start index, \emph{xpos} stop index, \emph{ypos} start index, \emph{ypos} stop index respectively.
This allows the calling method to quickly assess the \emph{shademap's} indexes which correspond to the respective tree. Additionally, it prevents the possibility of an \emph{ArrayIndexOutOfBound} exception. This may present if a tree is situated near the edge of the landscape and has a large extent such that start or end points of the tree correspond to array indexes outside the scope of the landscape.

The method using a simple comparison to evaluate the \emph{x, y} start and end positions. The \emph{x} bounds would be calculated as follows:
\begin{itemize}
\item \emph{x} start: $Math.max(0, xpos-extent)$
\item \emph{x} stop: $Math.min(dimX, xpos+ext)$
\end{itemize}
The same procedure is used to find the \emph{y} bounds.
		
%--------------------Land		
\subsection{Land} \label{class: land}
The \emph{Land} class is used to describe the piece of land on which the forest of trees grow. There is only one object of the \emph{Land} class which is passed between classes. The instance variables of the \emph{Land} class are:
\begin{itemize}
	\item \textbf{dimX}: the \emph{x} dimensions of the land map
	\item \textbf{dimY}: the \emph{y} dimensions of the land map
	\item \textbf{fullmap}: a 2-D array of floats representing the average amount of sunlight received by a square metre of land on the land map.
	\item \textbf{shademap}: a changing 2-D array of floats which represents the amount of sunlight at each canopy level of a simulation. After each simulation the \emph{shademap} is reset to the \emph{fullmap} to simulate moving back to the top of the canopy.
\end{itemize}

	\subsubsection{setShade()}\label{meth: setShade}
This method is used to write a new value to the \emph{shademap} at a specified \emph{x, y} location. This method directly accesses the shared memory resource \emph{shademap}, however it has not been synchronised. \emph{setShade()} is used as a helper method for the \emph{shadow()} method( see Section\,\ref{meth: shadow}) which locks on the shademap before this method is called.
 Therefore, the need for a reentrant lock is redundant and has been excluded. This safety precaution is omitted in an attempt to improve the program's speed of execution.

It should be noted that this method is also called from \emph{resetShade()}. However, the \emph{resetShade()} method is called after the multithreading process and therefore occurs sequentially, negating the need for any synchronisation.
	
	\subsubsection{resetShade()}\label{meth: resetShade}
This method is called once per simulated run after all threads have returned. It is used to reset the values of the \emph{shademap} according to the \emph{sunmap's} values for the next simulation cycle. This effectively moves the simulation back to the top of the tree canopy, above the shade of any tree where the full amount of sunlight is received.

The method simply loops through every array element of the \emph{shademap} and sets its value to the corresponding \emph{sunmap} value.

	\subsubsection{shadow()}\label{meth: shadow}
This method must be called after each tree has grown and is used to simulate the effects of sunlight filtration beneath a tree. When called, the method will reduce the \emph{shademap's} sunlight values beneath a tree by 1/10$^{th}$ of their original values. This method is called during the multithreading process. After each tree has been grown it must then cast shade onto the \emph{shademap}, and since many trees in a layer are processed at once, this method may be called concurrently by many threads.

Because this method requires read and write access to the shared memory resource \emph{shademap}, it has been synchronised. The method can be described by Algorithm\,\ref{alg: shadow}.

\begin{algorithm}
\floatname{algorithm}{Procedure}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
	\caption{Method used to cast shade on the \emph{shademap} beneath a tree.}
	\label{alg: shadow}
	\begin{algorithmic}
		\REQUIRE Tree object to determine where to cast shade on the \emph{shademap}
		\ENSURE Reduce cells of the shademap beneath the tree to 1/10th their value.
		\STATE getPositions() \COMMENT {Get the start and end indexes of the tree on the shademap}
		\FOR{every xpos}
			\STATE{Lock the shademap at row x}
			\FOR{every ypos}
				\STATE{shade $\Leftarrow$ getShade(xpos, ypos)}
				\STATE{newShade $\Leftarrow$ shade/10}
				\STATE{setShade(xpos, ypos, newShade)}
				\STATE{shademap[xpos][ypos] $\Leftarrow$ shademap[xpos][ypos]/10 }
			\ENDFOR
			\STATE{Unlock the shademap at row x}
		\ENDFOR
	\end{algorithmic}
\end{algorithm}


%--------------------Model
\subsection{Model}
The \emph{Model} object is used to represent the model of the system in a Model-View-Controller design. It stores the data of the system and is also used to update the system. The \emph{Model} object is described by the following instance variables:
\begin{itemize}
	\item \textbf{sundata}: A \emph{SunData} object which contains the \emph{sunmap} and list of \emph{Tree} objects which define the input landscape.
	\item \textbf{years}: Tracks the number of years a simulation has run for. This is also used to update the GUI display.
	\item \textbf{running}: An \emph{AtomicBoolean} variable which is used to determine the state of the GUI. When \emph{running=true}, the simulation is allowed to run. When \emph{running=false} the simulation is halted.
	\item \textbf{SEQUENTIAL\_CUTOFF}: Defines the point at which the \emph{Divide and Conquer} parallel algorithm switches over to sequential operation.
	\item \textbf{fjpool}: A \emph{ForkJoinPool} object used to invoke and manage the multithreading aspect of the program.
\end{itemize}

\subsubsection{simulate()}
This method is used to update the model. Each call to this method represents one simulation cycle. In each simulation cycle, the model considers every layer of the tree canopy starting from the largest trees down to the smallest trees. For each layer, the model invoke a ForkJoinPool of threads which use the \emph{Divide and Conquer} method to reduce the size of the \emph{Tree} array into portions which each thread then manages. The method waits for all threads in each layer to complete before considering the subsequent layers.

This method is explained in Algorithm\,\ref{alg: simulate}:
\begin{algorithm}
\floatname{algorithm}{Procedure}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
	\caption{Multithreaded model update method}
	\label{alg: simulate}
	\begin{algorithmic}
		\REQUIRE 
		\ENSURE Each layer of tree  canopy is considered sequentially. For each tree in the layer, grow the tree and then recast the tree's shade.
		\STATE minE $\Leftarrow$ 18
		\STATE maxE $\Leftarrow$ 20
		\FOR{Every layer of tree canpoy}
			\STATE{Invoke a ForkJoinPool of threads acting on every tree in this layer by instantiating a new \emph{Simulator} object}
			\STATE{maxE $\Leftarrow$ minE}
			\STATE{minE $\Leftarrow$ minE-2}
			\WHILE{Threads still active}
				\STATE{Wait}
			\ENDWHILE
		\ENDFOR
		\STATE years $\Leftarrow$ years+1
		\STATE Reset the shademap
	\end{algorithmic}
\end{algorithm}


%--------------------Simulator
\subsection{Simulator}
The \emph{Simulator} class extends the \emph{RecursiveAction} class and is used to implement the \emph{Divide and Conquer} method. It is invoked by the \emph{ForkJoinPool} of the \emph{Model} object and is then called recursively by itself as it segments the list of items to compute into smaller and smaller bits. This class is responsible for threading the program. A \emph{Simulator} object is described by the following instance variables:
\begin{itemize}
	\item \textbf{SEQUENTIAL\_CUTOFF}
	\item \textbf{lo, hi}: variables used to describe the start and end points of the input data array. This demarcates the data items from the input array which the active thread is handling.
	\item \textbf{trees}: the list of \emph{Tree} objects from the input data.
	\item \textbf{sunmap}: the \emph{Land} object used to describe the input landscape's sunlight distribution.
	\item  \textbf{sundata}: A \emph{SunData} object which contains the \emph{sunmap} and list of \emph{Tree} objects which define the input landscape.
	\item \textbf{minE and maxE}: floats used to define a range of \emph{Tree} object sizes. All \emph{Tree} objects within the specified range are part of the same layer.
\end{itemize}

\subsubsection{compute()}
This is a recursive method which is performed by many threads. The size of the \emph{Tree} array is bifurcated recursively and distributed to new threads until the array is within the sequential cutoff size. At this point, the sequential part of the algorithm grows each tree within the current layer and then casts shade on the \emph{shademap}. The method is described in Algorithm\,\ref{alg: compute}

\begin{algorithm}
\floatname{algorithm}{Procedure}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
	\caption{Divide and Conquer recursive method to update a layer of trees.}
	\label{alg: compute}
	\begin{algorithmic}
		\REQUIRE The \emph{sundata} object, sequential cutoff, tree layer, lo and hi values to segment the \emph{tree} array.
		\ENSURE Recursive calls to bifurcate the \emph{tree} array until within sequential cutoff, at which point the algorithm computes the tree growth and casts the tree's shade.
		\IF{hi-lo < sequential cutoff }
			\FOR{lo to hi of tree[]}
				\IF{tree in this layer}
					\STATE Lock the shademap
					\STATE grow the tree
					\STATE cast shade
					\STATE Unlock the shademap
				\ENDIF
			\ENDFOR
		\ELSE
			\STATE fork left on left half of tree array
			\STATE fork right on right half of tree array
		\ENDIF
	\end{algorithmic}
\end{algorithm}

Note that in this method, the \emph{shademap} is locked whilst each tree grows and shades the map. This is necessary to prevent any bad interleavings or race conditions. This is explained in more detail in Section\,\ref{sec: code}.

%--------------------Button Listener
\subsection{Button Listener}\label{class: btn}
The ButtonListener class fulfils the controller aspect of the MVC design. This simple action event handler is used to determine which of the GUI's buttons the user has pressed and to then instruct the model appropriately. The class uses a simple \emph{if-else} conditional block to categorise the input into the following commands: \emph{start}, \emph{pause}, \emph{end}, \emph{reset}.

\begin{itemize}
\item \textbf{Start Button:} \\
Set the model's \emph{running} to true. 
\item \textbf{Pause Button:} \\
Set the model's \emph{running} to false.
\item \textbf{End:}\\
Exit the program by calling \emph{System.exit(0)}
\item \textbf{Reset:}
	\begin{itemize}
		\item Set the model's \emph{running} to false.
		\item Set the model's \emph{years} to 0 and update the GUI's year label.
		\item For each tree in the model, set the extent to $0.4$.
	\end{itemize}
\end{itemize}

%==========================MODEL-VIEW-CONTROLLER
\section{Model-View-Controller Design}\label{sec: mvc}

A Model-View-Controller design splits the functioning of a program into separate parts with distinct functions. By limiting and specifying how each part of the system interacts with the others, the program becomes more robust as each part becomes more modular and more stand-alone. This means that parts of the system can be easily modified without breaking other parts of the system.

The MVC implementation of this program can be seen in Figure\,\ref{fig: mvc}.
\begin{figure}[h]
\begin{centering}
	\includegraphics[width = 0.7\textwidth]{./Figures/mvc.png}
	\caption{Model-View-Controller implementation for the tree grow simulation program.}
	\label{fig: mvc}
\end{centering}
\end{figure}

Figure\,\ref{fig: mvc} clearly indicate that the user can only ever indirectly access the model. The user's interaction with the system is limited to a set number of inputs available to them by the controller. These controller inputs are then used to update the model. In this case, the user can control the system by interacting with the GUI buttons whose functions are described in Section\,\ref{class: btn}. When the user presses a GUI button, the model is then updated by the controller in a safe and predictable manner. The update model is then output to the display by the view component of the MVC. In this way, the user can observe the effects of their input.

In this application, the model is comprised of the \emph{Model} class which stores the variables and parameters that are used to describe the system at a point in time. The variables primarily used to describe the model are: the \emph{shademap} and the \emph{tree} array of the \emph{SunData} object. Additionally, the model holds the number of years the simulation has run for.

The view component of the MVC is described primarily by the \emph{ForestPanel} object. This object has a method which is threaded and constantly updates the GUI based on the \emph{Tree} array of the model. Additionally, the \emph{TreeGrow} class could be considered part of the view component as it is used to set the label of the GUI.

%==========================CONCURRENCY
\section{Java Concurrency Features}\label{sec: conc_features}

Concurrency is a state which described two or more events occurring simultaneously. In parallel programming, concurrency describes the situation of multiple threads requiring access to a shared resource. In this application, the shared resource is memory; particularly the \emph{shademap} variable. It is important to note that the \emph{shademap} is a 2-D float array.

Throughout the application, multiple threads may each be trying to access the \emph{shademap} concurrently. Each of these threads is reading from and then writing to the shared memory address. This becomes an issue in the case that the thread's access to the \emph{shademap} is not co-ordinated such that some information may be overwritten by an interrupting thread, causing the information to be lost. This is known as a bad interleaving.

Java provides several tools to synchronise thread operation and prevent any interleaving. Some of these tools include: \emph{Atomic} variables and the \emph{synchronized} keyword.

\subsection{Atomic Variables}
An \emph{Atomic} variable implies that only one (atomic) operation can be performed on a variable of this type at a time. Each \emph{Atomic} variable has intrinsic \emph{set} and \emph{get} methods which must be used to access the variable. However, Java only provides four flavours of the \emph{Atomic} variables: boolean, int, long and reference.

This implies that the \emph{Atomic} variables are not entirely useful when trying to protect access to the \emph{shademap}. However, this program does utilise an \emph{Atomic Boolean} variable called \emph{running}. This is used to control the state of the model and although it is not accessed directly from a multithreaded environment, its atomicity implies that its state changes will be uninterrupted.
Therefore, when the controller sets the state of the program, its state change can be ensured.

\subsection{Synchronized Statements and Methods}

In order to provide a thread with exclusive access to a memory resource, the \emph{synchronized} keyword is used. It can be used in two contexts: as a method descriptor or as a statement.

When used as a method descriptor, the lock is placed on the method's object. Therefore, in the case of the \emph{Tree} objects protected method \emph{setExt()}: once called the lock is acquired at the methods opening curly brace and is then released at the methods closing curly brace.
\begin{lstlisting}[language =Java]
	public synchronized void setExt(float e){
		ext = e;
	}
\end{lstlisting}
This means that the method \emph{setExt()} has exclusive access to this \emph{Tree} object and all of its instance variables for the duration of the method.

Alternatively, the \emph{synchronized} keyword can be used as a block statement to protect only part of a method. This may be used to mitigate the overhead of acquiring a lock and minimise the resource starvation time while the object is locked. This is used several times in this application such as in the \emph{compute()} method of the \emph{Simulator} class:

\begin{lstlisting}[language = Java]
synchronized(sunmap.shademap){
                        trees[i].sungrow(sunmap); //grow tree[i]
                        sunmap.shadow(trees[i]); //shade map under tree[i]
                    }
\end{lstlisting}

In this case, the program locks on the \emph{shademap} for a portion of the method. During this time, a thread is guaranteed exclusive access to \emph{shademap} whilst the tree is grown and then shaded.

%==========================CODE DETAILS
\section{Code Details}\label{sec: code}
%Overview of the code that is used
This section will outline aspects of the code which have been written to prevent any concurrency issues as well as discussing the effects of protecting the code on execution time. The classes used in the program can be found in Appendix\,\ref{app: code}.


\subsection{Thread Safety and Synchronisation}
A multithreaded program is considered thread safe if all threads have access to a shared memory resource but are only allowed to access the resource via mutual exclusion or through atomic operations. This ensures that only a single thread can read from or write to a shared memory resource at a time.

The primary concern for this program involves guaranteeing that each tree is processed correctly and that the \emph{shademap} is correctly updated before another tree accesses this same part of the data. This implies that the \emph{shademap} must be locked for any read or write operation.

Consider the following example illustrated in Figure\,\ref{fig: bad_interleaving} where two trees are within the same layer and overlap somewhat. Each of the trees is processed by a different thread and their statements may execute in some random order. A block with a lock symbol indicates a synchronised block of statements on an associated memory resource (in this case the \emph{shademap}).

\begin{figure}[h]
\begin{centering}
	\includegraphics[width = 0.6\textwidth]{./Figures/bad_interleaving.png}
	\caption{Illustrated example of an bad interleaving between two threads processing two trees}
	\label{fig: bad_interleaving}
\end{centering}
\end{figure}

In either case, the threads each have exclusive access to the \emph{shademap} at any time. Therefore we can guarantee that during each block the thread execution is uninterrupted. However, in both cases, each thread reads the same sunlight values from the \emph{shademap} despite them overlapping. This is not permissible as each overlapping tree within the same layer must be processed consecutively. Additionally, in both cases, the value which Thread 1 has written to the \emph{shademap} is overwritten when Thread 2 completes. Therefore information has been lost and the program has a bad interleaving situation because of a race condition. Each thread is racing to change the output before the other can complete.

This problem is solved by placing a lock on the \emph{shademap} for the entire duration of the depicted thread execution. This is seen in the \emph{compute()} method of the \emph{Simulator class}. Therefore, Thread 2 would only be able to read from the \emph{shademap} once Thread 1 has written its result to the \emph{shademap}.

Unfortunately, preventing these race conditions forces threads to execute consecutively instead of concurrently. This has a significant effect on the speed of the program as it moves closer towards a sequential style of programming.

%Potentially have a way of distributing shared memory to local memory for each thread and then resolve the conflicts
Another race condition presents itself when the \emph{Reset} button is pressed. Even though the controller changes the state of the model to suspend the simulation, there may still be threads which are actively updating the trees and their extents. Therefore, when \emph{Reset} method is setting all tree's extents to 0.4, it is potentially in a multithreaded situation. This implies that a tree's extent could be reset but then overridden by an active thread which was halfway through updating a tree. This was seen on intermittent resets where some trees would not have been reset. To solve this problem, the \emph{setExt()} method of the \emph{Tree} class was synchronized.


It should be noted that the \emph{JLable} class is not thread safe. This means that one can only call methods such as \emph{JLabel.setText()} from the event dispatcher thread (EDT). This implies that some updates of the \emph{year} label would be overridden and missed. This race-condition is unnoticeable in this simulation for small data. However for large data sets the label updates intermittently and jumps many years between updates. A solution to this was not found.


\subsection{Liveness and Deadlock}

The liveness of a program refers to the its ability to execute rapidly and appear to be responsive. Clearly this is affected by the complexity of the code. A heavily protected parallel program enforces a more sequential style approach and therefore reduces the speed of execution. Additionally, the overheads involved in creating and managing the threading process of a parallel program imply that it should run slower than a purely sequential program.

The simulation is not immediately responsive and is not very lively when processing a large input file such as the provided \emph{sample\_input.txt} which describes a $3000\times3000$ landscape with a million trees.

Therefore, efforts were made to minimise the number of \emph{synchronized} sections of code. This means that anytime a calling method requires access to a shared resource, whenever possible the final called method was the only one synchronized.

The program is additionally slowed down by having to wait for each thread in a layer to complete before being able to continue to the next layer. This seems unavoidable and causes a significant slow down of the execution. 

Fundamentally, it seems that the only way to produce a significant speedup through concurrency is to be able to lock a portion of the \emph{shademap} at a time (ideally only the portion required by the respective tree) to allow other trees within the same layer to execute concurrently - except in the case of a tree overlap.

However, this was not fully achieved. A partial solution was implemented whereby only the row of the \emph{shademap} currently being accessed is locked by the \emph{synchronized} statement. This provided some speedup but introduces significant bad-interleaving issues for the situation where trees are overlapping. This is demonstrated in Section\,\ref{sec: validate}. The issues presented by the partial solution are accepted since it may represent a semi-realistic view of tree's in the same layer whose canopies intertwine and therefore could potentially receive the same amount of sunlight and because the speedup achieved by this partial solution is sorely needed.

Deadlock in a concurrent program describes the situation whereby a program is halted because part A of the program is locked and waiting for part B of the program to release its lock before it can continue. However, the part B can only release its lock when part A releases its lock. 
The \emph{synchronized} methods and statements used in this program automatically release their locks when their block of execution is exited for any reason. Additionally, the program was written to ensure no interdependencies on locking. For this reason, no deadlock issues were encountered.

%==========================VALIDATION
\section{Validation}\label{sec: validate}
%How the code is validated to prevent any race conditions from occuring as well as checking for erros
%create a small map with overlapping trees and test to see if there's a race condition.
%Test the speed of the program for running 1 year simulations and then simulating up to 100 years for parallel vs seq

The simulation can be assessed using two metrics: the relative speedup achieved by the program (relative to the sequential program) and the accuracy of the output.

The speed of the program is tested for multiple sizes of input data. For each data size the time taken to simulate 100 years is recorded. The data set is reset before the simulation is begun to ensure that all simulations start with the same sized trees. 
The results are tabulated in Table\,\ref{table: results}.
\begin{table}[h]
\caption{Simulation execution times for multiple input sizes for both sequential and parallel programs.}
\begin{center}
\begin{tabular}{|L{1.8cm}|L{2cm}|L{2cm} | L{2cm} | L{1.8cm} | L{2cm} | }\hline
\textbf{Input data Size} & \textbf{Number of Trees} &  \textbf{Sequential (s)} & \textbf{Sequential Average (s)}&\textbf{Parallel (s)} & \textbf{Parallel Average (s)}\\ \hline \hline
 3000$^2$& 1\,000\,000 & 12.799 & 0.1299 & 21.4850 & 0.2140 \\
 500$^2$& 10\,000 & 0.3630 & 0.0036 & 0.4579 & 0.0046 \\
 1500$^2$& 5\,00\,000 & 11.0859 & 0.1109 & 0.1561 & 15.6060 \\
\hline
\end{tabular}
\end{center}
\label{table: results}
\end{table}%

From these results it is clear that the sequential program is consistently faster than the parallel program. The only way that the parallel program achieves a similar speed is by removing the locks on the \emph{shadow()} and \emph{sunexposure()} methods, however even in this case the program is slower than the sequential counterpart.

In order to test the validity of the output, a special input file was created which describes a $5\times5$ landscape with consistent sunlight distribution of 100 throughout. There are then 5 trees which overlap perfectly and are all placed in the exact same spot. These trees also have the same extent. The program is then run for the parallel and sequential versions.
The trees are then output to a file to display the result after 1 simulated year. It is expected that the parallel output should match the sequential output although the order of trees may differ.

The sequential output is shown below:
\lstinputlisting{../out/correctSeqOutput_1yr.txt}
As expected, the trees have been calculated in order. The overlapping of trees is evident as each tree's extent only increases by a tenth of the previous tree.

The sequential cutoff for the parallel program was changed to 2 to allow multiple threads to work on the small sample size. The results of the parallel program are seen below.
\lstinputlisting{../out/parallelOutput_safe_1yr_SC_2.txt}
Although the order is jumbled, the output of the results is the same. This implies that the program functions correctly.

\section{Discussion}
This assignment has demonstrated that parallelisms and concurrency must be very carefully considered to produce an output which is both faster and correct. A number of different methods of solving the resource starvation problem which throttled the performance of the parallel program were conceptualised.

Future iterations of this program would attempt to lock a portion of the \emph{shademap} by creating a corresponding grid of locks for the \emph{shademap}. This would theoretically allow for higher throughput of threads and more concurrency in a safe manner as many threads may access the shademap at once and write to it concurrently so long a they are not writing to the same addresses. 

Alternatively, a method of segmenting the \emph{shademap} into grids, each represented by smaller 2-D arrays would allow for the same thing but with slightly fewer throughput depending on the grid size.



\newpage

%--------------------------------------------------------------------------------------------------------------------------------------------------
% Appendix
%--------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\section*{APPENDIX}
\setcounter{page}{1}
\renewcommand{\thepage}{\roman{page}}
\appendix
\setcounter{figure}{0} 	%figures start counting from zero again
\renewcommand\thefigure{\thesection.\arabic{figure}} 	% makes the numbers appear as Figure A.1:
\setcounter{table}{0} 		%tables start counting from zero again
\renewcommand\thetable{\thesection.\arabic{table}} 

\section{Code}\label{app: code}

The classes used to run the program are shown below.

\lstinputlisting[language=java]{../src/TreeGrow.java}
\lstinputlisting[language=java]{../src/Tree.java}
\lstinputlisting[language=java]{../src/Land.java}
\lstinputlisting[language=java]{../src/Simulator.java}
\lstinputlisting[language=java]{../src/Model.java}
\lstinputlisting[language=java]{../src/ForestPanel.java}
\lstinputlisting[language=java]{../src/ButtonListener.java}




\end{document}